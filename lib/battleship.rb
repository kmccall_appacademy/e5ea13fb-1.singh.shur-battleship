class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    return 'You destoyed all ships!' if game_over?
    puts 'Pick a square!'
    usr = nil
    loop do
      usr = @player.get_play
      break usr if @board.in_grid?(usr)
      puts 'bad entry!'
    end
    attack(usr)
  end
end
