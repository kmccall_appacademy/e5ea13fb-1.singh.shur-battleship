class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid = grid.nil? ? Board.default_grid : grid
  end

  def self.default_grid
    arr = []
    10.times { arr << [nil] * 10 }
    arr
  end

  def in_grid?(pos)
    r, c = pos
    r < @grid.length && c < @grid[0].length
  end

  def [](other)
    # other ex) [2, 4]
    r, c = other
    @grid[r][c]
  end

  def []=(key, other)
    # other ex) [2, 4]
    r, c = key
    @grid[r][c] = other
  end

  def count
    @grid.flatten.reduce(0) { |a, s| s == :s ? a + 1 : a }
  end

  def empty?(pos = nil)
    # pos example: [0, 3]
    return self[pos].nil? unless pos.nil?
    count.zero?
  end

  def full?(pos = nil)
    return self[pos].nil? unless pos.nil?
    count == @grid.length * @grid[0].length
  end

  def won?
    count.zero?
  end

  def place_random_ship
    raise 'full board' if full?
    r = rand(0...grid.length)
    c = rand(0...grid[0].length)
    self[[r, c]] = :s
  end
end
